package io.gitlab.artismarti

import org.springframework.http.MediaType
import org.springframework.http.client.reactive.ReactorClientHttpConnector
import org.springframework.web.client.reactive.ClientWebRequestBuilders
import org.springframework.web.client.reactive.ResponseExtractors
import org.springframework.web.client.reactive.WebClient
import java.time.Duration

/**
 * @author artur
 */
class HelloControllerTest {

	companion object {
		@JvmStatic
		fun main(vararg args: String) {
			val httpConnector = ReactorClientHttpConnector()
			val webClient = WebClient(httpConnector)

			val responseString = webClient.perform(ClientWebRequestBuilders.get("http://localhost:8888/hello")
					.accept(MediaType.APPLICATION_JSON)).extract(ResponseExtractors
					.body(String::class.java))

			val responseJson = webClient.perform(ClientWebRequestBuilders.get("http://localhost:8888/all")
					.accept(MediaType.APPLICATION_JSON)).extract(ResponseExtractors
					.body(String::class.java))

			println(responseString.block(Duration.ofSeconds(1L)))
			println(responseJson.block(Duration.ofSeconds(1L)))
		}
	}
}