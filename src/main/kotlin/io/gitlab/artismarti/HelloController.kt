package io.gitlab.artismarti

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

/**
 * @author artur
 */
@RestController
class HelloController {

	data class Person(val name: String)

	@GetMapping("/hello")
	fun hello(): Mono<String> {
		return Mono.just("Hello World!")
	}

	@GetMapping("/all")
	fun all(): Flux<Person> {
		return Flux.just(Person("Artur"), Person("Chris"))
	}

}