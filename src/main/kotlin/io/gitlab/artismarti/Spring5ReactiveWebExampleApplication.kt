package io.gitlab.artismarti

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
open class Spring5ReactiveWebExampleApplication

fun main(args: Array<String>) {
    SpringApplication.run(Spring5ReactiveWebExampleApplication::class.java, *args)
}
