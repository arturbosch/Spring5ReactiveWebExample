# Spring 5 Reactive Web Example with Kotlin

- Open the project in an ide like IntelliJ
- Start the main method in Spring5ReactiveWebExampleApplication
- Navigate to localhost:8888/all or .../hello in your browser to test the rest endpoints
- Run the main method in HelloControllerTest to test the reactive web client